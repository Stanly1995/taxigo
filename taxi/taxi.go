package taxi

import (
	"github.com/labstack/echo/v4"
	"taxigo/taxi/internal/app"
	"taxigo/taxi/internal/infrastrusture/http"
	"taxigo/taxi/internal/infrastrusture/inmemorepo"
	"taxigo/x/xlog"
)

type Service struct {
	ordersService app.OrdersService
	e             *echo.Echo
	loggerMaker   xlog.LoggerFactory
}

func NewService(e *echo.Echo, loggerMaker xlog.LoggerFactory) *Service {
	repo := inmemorepo.NewInMemoOrdersRepo()
	ordersService := app.NewOrderService(repo)
	return &Service{
		ordersService: ordersService,
		e:             e,
		loggerMaker:   loggerMaker,
	}
}

func (s *Service) GenerateOrders() {
	for {
		if s.ordersService.Count() >= 50 {
			break
		}
		err := s.ordersService.GenerateNew()
		if err != nil {
			panic(err)
		}
	}
}

func (s *Service) Start() {
	openCloser := app.NewOrdersOpenCloser(s.ordersService, s.loggerMaker)
	openCloser.Run()
}

// SetupHandlers initializes http handlers
func (s *Service) SetupHandlers() {
	oh := http.NewOrdersHandler(s.ordersService, s.loggerMaker)

	s.e.GET("/request", oh.ViewRandomActive)
	s.e.GET("/admin/requests", oh.GetAll)
}
