package inmemorepo

import (
	"sync"
	"taxigo/taxi/internal/domain"
	"taxigo/x/xtype"
)

// inMemoOrdersRepo implements domain.OrdersRepo interface
type inMemoOrdersRepo struct {
	orders map[xtype.Name]domain.Order
	mu     sync.RWMutex
}

// NewInMemoOrdersRepo is initializer for repository which stores info in memory
func NewInMemoOrdersRepo() domain.OrdersRepo {
	return &inMemoOrdersRepo{
		orders: map[xtype.Name]domain.Order{},
	}
}

// Save stores or redefines order
func (i *inMemoOrdersRepo) Save(order domain.Order) error {
	i.mu.Lock()
	i.orders[order.Name()] = order
	i.mu.Unlock()
	return nil
}

// GetFirst returns first order which meets requirements in input options
// If there is no needed orders, will be returned error ErrNeededOrderNotExisted
func (i *inMemoOrdersRepo) GetFirst(opts domain.OrderOptions) (*domain.Order, error) {
	i.mu.RLock()
	defer i.mu.RUnlock()
	for name := range i.orders {
		order := i.orders[name]
		if order.Active() == opts.Active {
			returnOrder := i.orders[name]
			return &returnOrder, nil
		}
	}

	return nil, domain.ErrNeededOrderNotExisted
}

// GetByName returns order by input name. If it is not existed, will be returned error
func (i *inMemoOrdersRepo) GetByName(name xtype.Name) (*domain.Order, error) {
	i.mu.RLock()
	order, ok := i.orders[name]
	i.mu.RUnlock()
	if !ok {
		return nil, domain.ErrNeededOrderNotExisted
	}
	return &order, nil
}

// GetAll returns all orders
func (i *inMemoOrdersRepo) GetAll() ([]domain.Order, error) {
	i.mu.RLock()
	orders := make([]domain.Order, 0, len(i.orders))
	for id := range i.orders {
		orders = append(orders, i.orders[id])
	}
	i.mu.RUnlock()

	return orders, nil
}

// Count returns count of orders in private orders map
func (i *inMemoOrdersRepo) Count() int {
	i.mu.RLock()
	count := len(i.orders)
	i.mu.RUnlock()

	return count
}
