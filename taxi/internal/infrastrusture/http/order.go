package http

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"taxigo/taxi/internal/app"
	"taxigo/taxi/internal/domain"
	"taxigo/x/xlog"
)

type Order struct {
	Name       string `json:"name,omitempty"`
	ViewsCount uint   `json:"views_count,omitempty"`
	Active     bool   `json:"active,omitempty"`
}

type OrdersHandler struct {
	ordersService app.OrdersService
	logger        xlog.Logger
}

func NewOrdersHandler(
	ordersService app.OrdersService,
	loggerMaker xlog.LoggerFactory,
) *OrdersHandler {
	return &OrdersHandler{
		ordersService: ordersService,
		logger:        loggerMaker.Create(""),
	}
}

func (oh *OrdersHandler) ViewRandomActive(ctx echo.Context) error {
	order, err := oh.ordersService.ViewRandomActive()
	if err != nil {
		return echo.ErrInternalServerError
	}
	return ctx.JSON(http.StatusOK, oh.toDtoActiveOrder(order))
}

func (oh *OrdersHandler) GetAll(ctx echo.Context) error {
	orders, err := oh.ordersService.GetAll()
	if err != nil {
		return echo.ErrInternalServerError
	}
	return ctx.JSONPretty(http.StatusOK, oh.toDtoAllOrders(orders), "  ")
}

func (oh *OrdersHandler) toDtoActiveOrder(order *domain.Order) *Order {
	return &Order{
		Name: order.Name().String(),
	}
}

func (oh *OrdersHandler) toDtoAllOrders(domainOrders []domain.Order) []Order {
	dtoOrders := make([]Order, 0, len(domainOrders))
	for i := range domainOrders {
		dtoOrders = append(dtoOrders, Order{
			Name:       domainOrders[i].Name().String(),
			Active:     domainOrders[i].Active(),
			ViewsCount: domainOrders[i].ViewsCount(),
		})
	}
	return dtoOrders
}
