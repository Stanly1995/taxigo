package domain

import (
	"taxigo/x/xerror"
	"taxigo/x/xtype"
)

// ErrOrderNotActive returned by Cancel function
const ErrOrderNotActive = xerror.New("order is not active")

// ErrOrderNotActive returned by Activate function
const ErrOrderActive = xerror.New("order is active")

// Order contains information about minified "taxi" order
type Order struct {
	name       xtype.Name
	viewsCount uint
	active     bool
}

// NewOrder is initializer for Order
func NewOrder(name xtype.Name) Order {
	return Order{
		name:   name,
		active: true,
	}
}

// Cancel sets order's active field to false. If orders was cancelled before it will return error
func (o *Order) Cancel() error {
	if !o.active {
		return ErrOrderNotActive
	}
	o.active = false
	return nil
}

// Active is getter for order's active field
func (o *Order) Active() bool {
	return o.active
}

// Activate sets order's active field to true. If orders was activated before it will return error
func (o *Order) Activate() error {
	if o.active {
		return ErrOrderActive
	}
	o.active = true
	return nil
}

// Name is getter for order's name field
func (o *Order) Name() xtype.Name {
	return o.name
}

func (o *Order) View() *Order {
	o.viewsCount++
	return o
}

func (o *Order) ViewsCount() uint {
	return o.viewsCount
}
