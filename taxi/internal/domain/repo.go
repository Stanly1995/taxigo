package domain

import (
	"taxigo/x/xerror"
	"taxigo/x/xtype"
)

// ErrNeededOrderNotExisted is returned when repo does not contain appropriate order
const ErrNeededOrderNotExisted = xerror.New("needed order is not existed")

// OrderOptions needed for orders getting
type OrderOptions struct {
	Active bool
}

// OrdersRepo stores data about orders
type OrdersRepo interface {
	Save(order Order) error
	GetFirst(opts OrderOptions) (*Order, error)
	GetByName(name xtype.Name) (*Order, error)
	GetAll() ([]Order, error)
	Count() int
}
