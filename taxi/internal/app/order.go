package app

import (
	"taxigo/taxi/internal/domain"
	"taxigo/x/xtype"
)

// OrdersService contains lodic for generating, cancelling and getting orders
type OrdersService interface {
	GenerateNew() error
	GenerateNewOrActivateRandom() error
	CancelRandom() error
	GetAll() ([]domain.Order, error)
	Count() int
	ViewRandomActive() (*domain.Order, error)
}

// ordersService implements OrdersService interface
type ordersService struct {
	ordersRepo domain.OrdersRepo
}

// NewOrderService is initializer for ordersService
func NewOrderService(ordersRepo domain.OrdersRepo) OrdersService {
	return &ordersService{
		ordersRepo: ordersRepo,
	}
}

// GenerateNew generates new order and saves it to repo
func (o *ordersService) GenerateNew() error {
	name := xtype.GenerateName()
	newOrder := domain.NewOrder(name)
	return o.ordersRepo.Save(newOrder)
}

// GenerateNewOrActivateRandom generates new order, or activates cancelled order
func (o *ordersService) GenerateNewOrActivateRandom() error {
	name := xtype.GenerateName()
	order, err := o.ordersRepo.GetByName(name)
	var newOrder domain.Order
	switch err {
	case nil:
		if !order.Active() {
			err = order.Activate()
			newOrder = *order
		} else {
			canceledOrder, err := o.ordersRepo.GetFirst(domain.OrderOptions{Active: false})
			if err != nil {
				return err
			}
			err = canceledOrder.Activate()
			newOrder = *canceledOrder
		}
	case domain.ErrNeededOrderNotExisted:
		newOrder = domain.NewOrder(name)
		err = nil
	}
	if err != nil {
		return err
	}

	err = o.ordersRepo.Save(newOrder)
	if err != nil {
		return err
	}

	return nil
}

// CancelRandom cancels random order
func (o *ordersService) CancelRandom() error {
	order, err := o.ordersRepo.GetFirst(domain.OrderOptions{Active: true})
	if err != nil {
		return err
	}
	err = order.Cancel()
	if err != nil {
		return err
	}

	return o.ordersRepo.Save(*order)
}

// ViewRandomActive returns random active order
func (o *ordersService) ViewRandomActive() (*domain.Order, error) {
	order, err := o.ordersRepo.GetFirst(domain.OrderOptions{Active: true})
	if err != nil {
		return nil, err
	}
	order.View()

	return order, o.ordersRepo.Save(*order)
}

// GetAll returns all orders in system
func (o *ordersService) GetAll() ([]domain.Order, error) {
	return o.ordersRepo.GetAll()
}

// GetAll returns count of all orders in system
func (o *ordersService) Count() int {
	return o.ordersRepo.Count()
}
