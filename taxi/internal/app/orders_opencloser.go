package app

import (
	"taxigo/x/xlog"
	"time"
)

const DelayInMsec = 200

type OrdersOpenCloser struct {
	ordersService OrdersService
	logger        xlog.Logger
}

func NewOrdersOpenCloser(
	ordersService OrdersService,
	loggerMaker xlog.LoggerFactory,
) *OrdersOpenCloser {
	return &OrdersOpenCloser{
		ordersService: ordersService,
		logger:        loggerMaker.Create(""),
	}
}

func (ooc *OrdersOpenCloser) Run() {
	go func() {
		ticker := time.NewTicker(time.Millisecond * DelayInMsec)
		for {
			select {
			case <-ticker.C:
				if err := ooc.processOrders(); err != nil {
					ooc.logger.Error("Orders opening/closing has been finished with error: ", err)
				}
			}
		}
	}()
}

func (ooc *OrdersOpenCloser) processOrders() error {
	err := ooc.ordersService.GenerateNewOrActivateRandom()
	if err != nil {
		return err
	}

	return ooc.ordersService.CancelRandom()
}
