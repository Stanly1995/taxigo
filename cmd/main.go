package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"taxigo/pkg/config"
	"taxigo/taxi"
	"taxigo/x/xlog"
)

const (
	logFmt     = `{"time":"${time_rfc3339_nano}","level":"${level}", "at":"${long_file}:${line}"}`
	httpLogFmt = `{"time":"${time_rfc3339_nano}","level":"HTTP","message":"${remote_ip}` +
		` > ${method} ${path} > ${status}"}` + "\n"
)

func main() {
	cfg, err := config.GetConfig()
	if err != nil {
		panic(err)
	}

	loggerMaker := xlog.NewLoggerFactory(logFmt, xlog.Lvl(cfg.LogLevel))
	mainLogger := loggerMaker.Create("")
	mainLogger.Info(xlog.PrintLoggingLevel(xlog.Lvl(cfg.LogLevel)))

	e := echo.New()
	e.HideBanner = true
	e.Logger = loggerMaker.Create("")
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: httpLogFmt,
	}))
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{
			echo.HeaderOrigin,
			echo.HeaderContentType,
			echo.HeaderAccept,
			echo.HeaderAuthorization,
		},
	}))

	taxiService := taxi.NewService(e, loggerMaker)
	taxiService.SetupHandlers()
	taxiService.GenerateOrders()
	taxiService.Start()

	if err := e.Start(cfg.ListenAddr); err != nil {
		mainLogger.Fatal(err)
	}
}
