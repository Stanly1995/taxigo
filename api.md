# taxigo service description 

## Get random order

>Method: Get
>
>Path: **/request**
>
>Response Body:

```json5
{
  "name":"ec"
}
```

## Get all orders

>Method: Get
>
>Path: **/admin/requests**
>
>Response Body:

```json5
[
  {
    "name":"ec" // inactive order without views
  },
  {
      "name": "gp", // inactive order with views
      "views_count": 1173
  },
  {
        "name": "ep", // active order with views
        "views_count": 1133,
        "active": "true"
  },
]
```