package itesting

import (
	"context"
	"testing"

	"gitea.astarpay.loc/ipsp-x/ipsp/internal/x/xtype"
	"github.com/jmoiron/sqlx"
	// For testing purposes
	_ "github.com/lib/pq"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
)

const pgImage = "postgres:12.1-alpine"
const pgPortProto = "5432/tcp"

const pgUser = "postgres"
const pgPass = "test"

func PreparePgDB(t *testing.T, dbName string) (dsn string, db *sqlx.DB, terminate func()) {
	if dbName == "" {
		t.Error("db name is empty")
	}
	ctx := context.Background()
	req := testcontainers.ContainerRequest{
		Image:        pgImage,
		ExposedPorts: []string{pgPortProto},
		Env: map[string]string{
			"POSTGRES_USER":     pgUser,
			"POSTGRES_PASSWORD": pgPass,
			"POSTGRES_DB":       dbName,
		},
		WaitingFor: wait.ForListeningPort(pgPortProto),
	}
	pgC, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})
	if err != nil {
		t.Fatal(err)
	}

	host, err := pgC.Host(ctx)
	if err != nil {
		t.Fatal(err)
	}
	port, err := pgC.MappedPort(ctx, pgPortProto)
	if err != nil {
		t.Fatal(err)
	}
	dsn = xtype.FormatDSN(pgUser, pgPass, host, port.Port(), dbName)
	db, err = sqlx.Open("postgres", dsn)
	if err != nil {
		t.Fatal(err)
	}
	terminate = func() {
		_ = db.Close()
		if err := pgC.Terminate(ctx); err != nil {
			t.Fatal(err)
		}
	}
	return dsn, db, terminate
}
