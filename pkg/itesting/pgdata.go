package itesting

import (
	"gitea.astarpay.loc/ipsp-x/ipsp/migrations"
	"github.com/golang-migrate/migrate/v4"
	// For testing purposes
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	bindata "github.com/golang-migrate/migrate/v4/source/go_bindata"
	"github.com/jmoiron/sqlx"
	"os"
	"testing"
	// For testing purposes
	_ "github.com/lib/pq"

	"github.com/romanyx/polluter"
)

func ApplyPgScheme(t *testing.T, dsn string) {
	s := bindata.Resource(migrations.AssetNames(), migrations.Asset)
	d, err := bindata.WithInstance(s)
	if err != nil {
		t.Fatal(err)
	}
	m, err := migrate.NewWithSourceInstance("go-bindata", d, dsn)
	if err != nil {
		t.Fatal(err)
	}
	if err = m.Up(); err != nil {
		if err != migrate.ErrNoChange {
			t.Fatal(err)
		}
	}
}

func SeedPgData(t *testing.T, db *sqlx.DB, filename string) {
	seed, err := os.Open(filename)
	if err != nil {
		t.Fatalf("failed to open seed file: %s", err)
	}
	defer seed.Close()
	p := polluter.New(polluter.PostgresEngine(db.DB))
	if err := p.Pollute(seed); err != nil {
		t.Fatalf("failed to pollute: %s", err)
	}
}
