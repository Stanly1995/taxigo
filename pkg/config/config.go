package config

import (
	"flag"
	"github.com/tkanos/gonfig"
)

type Config struct {
	ListenAddr string `env:"LISTEN_ADDR"`
	LogLevel   uint   `env:"LOG_LVL"`
}

// configPath returns path to config
// path is set in -c flag
// if -c flag is empty, "chat.json" will be used
func configPath() (confPath string) {
	flag.StringVar(&confPath, "c", "", "path to a configuration file")
	flag.Parse()
	return
}

// GetConfig  returns Config from config file
func GetConfig() (Config, error) {
	var c Config
	err := gonfig.GetConf(configPath(), &c)
	return c, err
}
