package xlog

import (
	"github.com/labstack/gommon/log"
	"io"
	"strconv"
)

type Lvl uint8

const (
	DEBUG Lvl = iota + 1
	INFO
	WARN
	ERROR
	OFF
)

func PrintLoggingLevel(l Lvl) string {
	msgPrefix := "Loging level is set to "
	switch l {
	case DEBUG:
		return msgPrefix + "1:DEBUG"
	case INFO:
		return msgPrefix + "2:INFO"
	case WARN:
		return msgPrefix + "3:WARN"
	case ERROR:
		return msgPrefix + "4:ERROR"
	case OFF:
		return msgPrefix + "5:OFF (panic and fatal only)"
	default:
		return msgPrefix + strconv.Itoa(int(l))
	}
}

type LoggerFactory interface {
	Create(prefix string) Logger
}

type loggerMaker struct {
	header string
	level  Lvl
}

func NewLoggerFactory(header string, level Lvl) LoggerFactory {
	return &loggerMaker{header: header, level: level}
}

// Logger prefix can be omitted
func (lm *loggerMaker) Create(prefix string) Logger {
	l := log.New(prefix)
	if lm.header != "" {
		l.SetHeader(lm.header)
	}
	l.SetLevel(log.Lvl(lm.level))
	return l
}

type Logger interface {
	Output() io.Writer
	SetOutput(w io.Writer)
	Prefix() string
	SetPrefix(p string)
	Level() log.Lvl
	SetLevel(v log.Lvl)
	SetHeader(h string)
	Print(i ...interface{})
	Printf(format string, args ...interface{})
	Printj(j log.JSON)
	Debug(i ...interface{})
	Debugf(format string, args ...interface{})
	Debugj(j log.JSON)
	Info(i ...interface{})
	Infof(format string, args ...interface{})
	Infoj(j log.JSON)
	Warn(i ...interface{})
	Warnf(format string, args ...interface{})
	Warnj(j log.JSON)
	Error(i ...interface{})
	Errorf(format string, args ...interface{})
	Errorj(j log.JSON)
	Fatal(i ...interface{})
	Fatalj(j log.JSON)
	Fatalf(format string, args ...interface{})
	Panic(i ...interface{})
	Panicj(j log.JSON)
	Panicf(format string, args ...interface{})
}
