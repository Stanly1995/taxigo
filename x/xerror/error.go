package xerror

type New string

func (e New) Error() string {
	return string(e)
}
