package xtype

import (
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

const letters = "abcdefghijklmnopqrstuvwxyz"

type Name string

func (id Name) String() string {
	return string(id)
}

func GenerateName() Name {
	b := make([]byte, 2)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return Name(b)
}
