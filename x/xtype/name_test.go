package xtype

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGenerateID(t *testing.T) {
	// act
	id := GenerateName()
	// assert
	assert.Equal(t, 2, len(id.String()))
}
