module taxigo

go 1.13

require (
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/golang-migrate/migrate/v4 v4.10.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/labstack/echo/v4 v4.1.16
	github.com/labstack/gommon v0.3.0
	github.com/lib/pq v1.3.0
	github.com/romanyx/polluter v1.2.2
	github.com/stretchr/testify v1.5.1
	github.com/testcontainers/testcontainers-go v0.5.0
	github.com/tkanos/gonfig v0.0.0-20181112185242-896f3d81fadf
)
