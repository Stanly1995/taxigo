FROM golang as builder

RUN mkdir /app
WORKDIR /app
COPY go.mod .
COPY go.sum .

RUN go mod download
COPY . .

RUN cd cmd && CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o taxigo
FROM scratch
COPY --from=builder /app/cmd/taxigo ./taxigo
ENV LISTEN_ADDR ":8888"
ENV LOG_LVL 1
EXPOSE 8888
ENTRYPOINT ["/taxigo"]